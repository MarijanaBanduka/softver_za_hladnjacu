﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(SoftverZaHladnjacu.Client.Startup))]
namespace SoftverZaHladnjacu.Client
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
