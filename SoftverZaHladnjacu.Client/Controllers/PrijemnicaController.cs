﻿using SoftverZaHladnjacu.Domain;
using SoftverZaHladnjacu.Domain.Repository;
using SoftverZaHladnjacu.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SoftverZaHladnjacu.Client.Controllers
{
    [Authorize]
    public class PrijemnicaController : Controller
    {
        private IPrijemnicaRepository _prijemnicaRepository;
        private IPoslovniPartnerRepository _poslovniPartnerRepository;
        private ISortaRepository _sortaRepository;
        private IHladnjacaRepository _hladnjacaRepository;
        private IPakovanjeRepository _pakovanjeRepository;
        public PrijemnicaController()
        {
            _prijemnicaRepository = new PrijemnicaRepository();
            _poslovniPartnerRepository = new PoslovniPartnerRepository();
            _sortaRepository = new SortaRepository();
            _hladnjacaRepository = new HladnjacaRepository();
            _pakovanjeRepository = new PakovanjeRepository();
        }

        // GET: Prijemnica
        public ActionResult Index()
        {
            ViewBag.Sorte = _sortaRepository.GetAll();
            return View();
        }

        public ActionResult GetPrijemnicasById(int id)
        {
            return PartialView("Prijemnice", _prijemnicaRepository.GetListById(id));
        }

        public ActionResult GetPrijemnicasByDate(DateTime date)
        {
            return PartialView("Prijemnice", _prijemnicaRepository.GetByDate(date));
        }

        public ActionResult GetPrijemnicasBySorta(int id)
        {
            return PartialView("Prijemnice", _prijemnicaRepository.GetBySorta(id));
        }

        public ActionResult GetPrijemnicasToBeStored()
        {
            return PartialView("Prijemnice", _prijemnicaRepository.GetAllNotStored());
        }

        public ActionResult GetAvailableCapacity()
        {
            return PartialView("Kapaciteti", _hladnjacaRepository.GetAllTypeGlavna());
        }

        public ActionResult Create()
        {
            ViewBag.Dobavljaci = _poslovniPartnerRepository.GetDobavljaci();
            ViewBag.Hladnjace = _hladnjacaRepository.GetAllTypePrijemna();
            ViewBag.Sorte = _sortaRepository.GetAll();

            return View();
        }

        [HttpPost]
        public ActionResult Create(PrijemnicaBO prijemnicaBO)
        {
            HladnjacaBO hladnjacaBO = _hladnjacaRepository.GetById(prijemnicaBO.Hladnjaca.Id);
            ViewBag.Dobavljaci = _poslovniPartnerRepository.GetDobavljaci();
            ViewBag.Hladnjace = _hladnjacaRepository.GetAllTypePrijemna();
            ViewBag.Sorte = _sortaRepository.GetAll();
            if (hladnjacaBO.Kapacitet < prijemnicaBO.Kolicina)
            {
                ModelState.AddModelError("Hladnjaca.Id", "Kapacitet odabrane hladnjače manji je od unete količine.");
                return View(prijemnicaBO);
            }
            if (_prijemnicaRepository.Create(prijemnicaBO) > 0)
            {
                ViewBag.MessageS = "Prijemnica je kreirana";
                return View("Index");
            } 
            else
            {
                ViewBag.MessageF = "Došlo je do greške, prijemnica nije kreirana!";
                return View(prijemnicaBO);
            }
        }

        public ActionResult Edit(int id)
        {
            if (_prijemnicaRepository.UpdateSetStored(id) > 0)
            {
                ViewBag.Sorte = _sortaRepository.GetAll();
                ViewBag.MessageS = "Prijemnica je ažurirana";
                return View("Index");
            }
            else
            {
                ViewBag.MessageF = "Došlo je do greške, prijemnica nije ažurirana!";
                return View("Index");
            }
        }
    }
}