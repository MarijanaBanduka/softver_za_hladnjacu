﻿using SoftverZaHladnjacu.Domain;
using SoftverZaHladnjacu.Domain.Repository;
using SoftverZaHladnjacu.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SoftverZaHladnjacu.Client.Controllers
{
    [Authorize]
    public class OtpremnicaController : Controller
    {
        private IOtpremnicaRepository _otpremnicaRepository;
        private IPoslovniPartnerRepository _poslovniPartnerRepository;
        private IPakovanjeRepository _pakovanjeRepository;
        public OtpremnicaController()
        {
            _otpremnicaRepository = new OtpremnicaRepository();
            _poslovniPartnerRepository = new PoslovniPartnerRepository();
            _pakovanjeRepository = new PakovanjeRepository();
        }
        // GET: Otpremnica
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult GetAllOtpremnicas()
        {
            return PartialView("Otpremnice", _otpremnicaRepository.GetAll());
        }

        public ActionResult GetOtpremnicasById(int id)
        {
            return PartialView("Otpremnice", _otpremnicaRepository.GetListById(id));
        }

        public ActionResult Create()
        {
            ViewBag.Kupci = _poslovniPartnerRepository.GetKupci();
            return View();
        }

        [HttpPost]
        public ActionResult Create(OtpremnicaBO otpremnicaBO)
        {
            var postedValues = Request.Form.GetValues("pakovanje");
            List<int> listaSifri = new List<int>();
            ViewBag.Kupci = _poslovniPartnerRepository.GetKupci();
            foreach (string value in postedValues)
            {
                if(value != null && value != "")
                {
                    try
                    {
                        int sifra = Convert.ToInt32(value);
                        listaSifri.Add(sifra);
                    }
                    catch (Exception)
                    {
                        ViewBag.MessageF = "Došlo je do greške, uneta je pogrešna šifra pakovanja!";
                        return View();
                    }
                }
            }
            if(listaSifri.Count <= 0)
            {
                ViewBag.MessageF = "Morate uneti stavke otpremnice!";
                return View();
            }
            otpremnicaBO.ListaSifri = listaSifri;
            int created = _otpremnicaRepository.Create(otpremnicaBO);
            if (created > 0)
            {
                ViewBag.MessageS = "Otpremnica je kreirana";
                return View("Index");
            } else
            {
                ViewBag.MessageF = "Došlo je do greške, otpremnica nije kreirana!";
                return View();
            }
        }

        public ActionResult Details(int id)
        {
            OtpremnicaBO otpremnicaBO = _otpremnicaRepository.GetById(id);
            return View(otpremnicaBO);
        }


        public ActionResult Edit(int id)
        {
            if (_otpremnicaRepository.Update(id) > 0)
            {
                ViewBag.MessageS = "Otpremnica je stornirana";
                return View("Index");
            }
            else
            {
                ViewBag.MessageF = "Došlo je do greške, otpremnica nije stornirana!";
                return View("Index");
            }
        }
    }
}