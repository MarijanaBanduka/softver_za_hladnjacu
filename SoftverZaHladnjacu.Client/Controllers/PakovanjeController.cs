﻿using SoftverZaHladnjacu.Domain;
using SoftverZaHladnjacu.Domain.Repository;
using SoftverZaHladnjacu.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SoftverZaHladnjacu.Client.Controllers
{
    [Authorize]
    public class PakovanjeController : Controller
    {
        private IPakovanjeRepository _pakovanjeRepository;
        private IHladnjacaRepository _hladnjacaRepository;
        private IPrijemnicaRepository _prijemnicaRepository;
        private ISortaRepository _sortaRepository;
        public PakovanjeController()
        {
            _pakovanjeRepository = new PakovanjeRepository();
            _hladnjacaRepository = new HladnjacaRepository();
            _prijemnicaRepository = new PrijemnicaRepository();
            _sortaRepository = new SortaRepository();
        }
        // GET: Pakovanje
        public ActionResult Index()
        {
            ViewBag.Hladnjace = _hladnjacaRepository.GetAllTypeGlavna();
            ViewBag.Klase = _pakovanjeRepository.GetAllKlasa();
            ViewBag.Sorte = _sortaRepository.GetAll();
            return View();
        }

        public int CheckId(int id)
        {
            PakovanjeBO pakovanje = _pakovanjeRepository.GetById(id);
            if(pakovanje != null && pakovanje.Id > 0 && pakovanje.DatumIzdavanja == null)
            {
                return 1;
            }
            return 0;
        }

        public ActionResult GetPakovanja()
        {
            return PartialView("Pakovanja", _pakovanjeRepository.GetAll());
        }

        public ActionResult GetPakovanjaByHladnjaca(int id)
        {
            return PartialView("Pakovanja", _pakovanjeRepository.GetPakovanjaByHladnjaca(id));
        }

        public ActionResult GetPakovanjaByKlasa(int id)
        {
            return PartialView("Pakovanja", _pakovanjeRepository.GetPakovanjaByKlasa(id));
        }
        public ActionResult GetPakovanjaBySorta(int id)
        {
            return PartialView("Pakovanja", _pakovanjeRepository.GetPakovanjaBySorta(id));
        }


        public ActionResult Create(int id)
        {
            ViewBag.Prijemnica = _prijemnicaRepository.GetById(id);
            ViewBag.Klase = _pakovanjeRepository.GetAllKlasa();
            ViewBag.Hladnjace = _hladnjacaRepository.GetAllTypeGlavna();
            return View();
        }

        [HttpPost]
        public ActionResult Create(PakovanjeBO pakovanjeBO)
        {
            HladnjacaBO hladnjacaBO = _hladnjacaRepository.GetById(pakovanjeBO.Hladnjaca.Id);
            PrijemnicaBO prijemnicaBO = _prijemnicaRepository.GetById(pakovanjeBO.Id);
            ViewBag.Klase = _pakovanjeRepository.GetAllKlasa();
            ViewBag.Hladnjace = _hladnjacaRepository.GetAllTypeGlavna();
            ViewBag.Prijemnica = prijemnicaBO;
            if (prijemnicaBO.SkladisniKalo < pakovanjeBO.Kolicina)
            {
                ModelState.AddModelError("Kolicina", "Količina za pakovanje mora biti manja od količine u hladnjači.");
                return View();
            }
            if (hladnjacaBO.Kapacitet < pakovanjeBO.Kolicina)
            {
                ModelState.AddModelError("Hladnjaca.Id", "Kapacitet odabrane hladnjače mora biti veći od unete količine.");
                return View();
            }
            int created = _pakovanjeRepository.Create(pakovanjeBO);
            if (created > 0)
            {
                ViewBag.MessageS = "Pakovanje je kreirano";
                List<HladnjacaBO> listaHladnjaca = (List<HladnjacaBO>)_hladnjacaRepository.GetAllTypeGlavna();
                foreach(HladnjacaBO hladnjaca in listaHladnjaca)
                {
                    if(hladnjaca.Id == hladnjacaBO.Id)
                    {
                        hladnjaca.Kapacitet -= pakovanjeBO.Kolicina;
                    }
                }
                prijemnicaBO.SkladisniKalo -= pakovanjeBO.Kolicina;
                ViewBag.Hladnjace = listaHladnjaca;
                ViewBag.Prijemnica = prijemnicaBO;
                return View();
            }
            else
            {
                ViewBag.MessageF = "Došlo je do greške, pakovanje nije kreirano";
                return View();
            }
        }

    }
}