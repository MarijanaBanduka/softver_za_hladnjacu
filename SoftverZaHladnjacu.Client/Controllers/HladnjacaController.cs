﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.UI;
using SoftverZaHladnjacu.Domain;
using SoftverZaHladnjacu.Domain.Repository;
using SoftverZaHladnjacu.EntityFramework;

namespace SoftverZaHladnjacu.Client.Controllers
{
    [Authorize(Roles = "Manager")]
    public class HladnjacaController : Controller
    {
        private IHladnjacaRepository _hladnjacaRepository;
        public HladnjacaController()
        {
            _hladnjacaRepository = new HladnjacaRepository();
        }

        // GET: Hladnjaca
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult GetAllHladnjacas()
        {
            return PartialView("Hladnjace", _hladnjacaRepository.GetAll());
        }

        public ActionResult Create()
        {
            ViewBag.Tipovi = _hladnjacaRepository.GetAllTypes();
            return View();
        }

        [HttpPost]
        public ActionResult Create(HladnjacaBO hladnjacaBO)
        {
            if(_hladnjacaRepository.Create(hladnjacaBO) > 0)
            {
                return Content("<script> alert('Hladnjača je kreirana'); window.location = '/Hladnjaca/Create' </script>");
            }
            else
            {
                ViewBag.Tipovi = _hladnjacaRepository.GetAllTypes();
                ViewBag.MessageF = "Došlo je do greške, hladnjača nije kreirana!";
                return View(hladnjacaBO);
            }
        }

        public ActionResult Edit(int id)
        {
            HladnjacaBO hladnjacaBO = _hladnjacaRepository.GetById(id);
            return View(hladnjacaBO);
        }

        [HttpPost]
        public ActionResult Edit(HladnjacaBO hladnjacaBO)
        {
            if(_hladnjacaRepository.Update(hladnjacaBO) > 0)
            {
                ViewBag.MessageS = "Hladnjača je ažurirana";
                return View("Index");
            }
            else
            {
                ViewBag.MessageF = "Došlo je do greške, hladnjača nije ažurirana!";
                return View(hladnjacaBO);
            }
        }
    }
}