﻿using SoftverZaHladnjacu.Domain;
using SoftverZaHladnjacu.Domain.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SoftverZaHladnjacu.EntityFramework
{
    public class UserRepository : IUserRepository
    {
        private StorageDatabaseEntities databaseEntities = new StorageDatabaseEntities();
        public UserBO GetUserByUsername(string username)
        {
            AspNetUser aspNetUser = databaseEntities.AspNetUsers.Single(t => t.UserName == username);
            return Map(aspNetUser);
        }

        public UserBO Map(AspNetUser aspNetUser)
        {
            UserBO userBO = new UserBO
            {
                Id = aspNetUser.Id,
                Ime = aspNetUser.FirstName,
                Prezime = aspNetUser.LastName,
                JMBG = aspNetUser.JMBG,
                Email = aspNetUser.Email
            };
            return userBO;
        }
    }
}
