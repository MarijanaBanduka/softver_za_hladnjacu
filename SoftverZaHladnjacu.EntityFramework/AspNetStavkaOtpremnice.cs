//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SoftverZaHladnjacu.EntityFramework
{
    using System;
    using System.Collections.Generic;
    
    public partial class AspNetStavkaOtpremnice
    {
        public int Id { get; set; }
        public int Rb { get; set; }
        public int IdOtpremnica { get; set; }
        public int IdPakovanje { get; set; }
    
        public virtual AspNetOtpremnica AspNetOtpremnica { get; set; }
        public virtual AspNetPakovanje AspNetPakovanje { get; set; }
    }
}
