﻿using SoftverZaHladnjacu.Domain;
using SoftverZaHladnjacu.Domain.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace SoftverZaHladnjacu.EntityFramework
{
    public class OtpremnicaRepository : IOtpremnicaRepository
    {
        private StorageDatabaseEntities databaseEntities = new StorageDatabaseEntities();
        private StavkaOtpremniceRepository stavkaOtpremniceRepository = new StavkaOtpremniceRepository();
        private UserRepository userRepository = new UserRepository();
        private PakovanjeRepository pakovanjeRepository = new PakovanjeRepository();
        private HladnjacaRepository hladnjacaRepository = new HladnjacaRepository();
        public int Create(OtpremnicaBO otpremnicaBO)
        {
            int saved = 0;
            int created = 0;
            AspNetOtpremnica aspNetOtpremnica = null;
            List<PakovanjeBO> listaPakovanja = new List<PakovanjeBO>();
            try
            {
                string username = HttpContext.Current.User.Identity.Name;
                UserBO userBO = userRepository.GetUserByUsername(username);
                aspNetOtpremnica = new AspNetOtpremnica
                {
                    Datum = DateTime.Now,
                    IdRadnik = userBO.Id,
                    IdPoslovniPartner = otpremnicaBO.PoslovniPartner.Id
                };
                databaseEntities.AspNetOtpremnicas.Add(aspNetOtpremnica);
                saved = databaseEntities.SaveChanges();
                if (saved > 0)
                {

                    foreach(int sifra in otpremnicaBO.ListaSifri)
                    {
                        PakovanjeBO pakovanjeBO = pakovanjeRepository.GetById(sifra);
                        listaPakovanja.Add(pakovanjeBO);
                        int updatedPakovanje = pakovanjeRepository.Update(pakovanjeBO.Id, DateTime.Now);
                        if(updatedPakovanje > 0)
                        {
                            HladnjacaBO hladnjacaBO = hladnjacaRepository.GetById(pakovanjeBO.Hladnjaca.Id);
                            double newCapacity = hladnjacaBO.Kapacitet + pakovanjeBO.Kolicina;
                            int updatedHladnjaca = hladnjacaRepository.UpdateCapacity(hladnjacaBO.Id, newCapacity);
                            if (updatedHladnjaca <= 0)
                            {
                                throw new Exception();
                            }
                        } else
                        {
                            throw new Exception();
                        }
                    }
                    created = stavkaOtpremniceRepository.Create(otpremnicaBO.ListaSifri, aspNetOtpremnica.Id);
                }
                return created;
            }
            catch (Exception)
            {
                if(saved > 0)
                {
                    databaseEntities.AspNetOtpremnicas.Remove(aspNetOtpremnica);
                    databaseEntities.SaveChanges();
                }
                if(listaPakovanja.Count > 0)
                {
                    foreach(PakovanjeBO pakovanjeBO in listaPakovanja)
                    {
                        pakovanjeRepository.Update(pakovanjeBO.Id, null);
                        hladnjacaRepository.UpdateCapacity(pakovanjeBO.Hladnjaca.Id, pakovanjeBO.Hladnjaca.Kapacitet);
                    }
                }
                return created;
            }
        }

        public IEnumerable<OtpremnicaBO> GetAll()
        {
            List<OtpremnicaBO> listaOtpremnica = new List<OtpremnicaBO>();
            foreach (AspNetOtpremnica aspNetOtpremnica in databaseEntities.AspNetOtpremnicas)
            {
                listaOtpremnica.Add(Map(aspNetOtpremnica));
            }
            return listaOtpremnica;
        }

        public OtpremnicaBO GetById(int id)
        {
            AspNetOtpremnica aspNetOtpremnica = databaseEntities.AspNetOtpremnicas.Single(t => t.Id == id);
            return Map(aspNetOtpremnica);
        }

        public IEnumerable<OtpremnicaBO> GetListById(int id)
        {
            List<OtpremnicaBO> listaOtpremnica = new List<OtpremnicaBO>();
            foreach (AspNetOtpremnica aspNetOtpremnica in databaseEntities.AspNetOtpremnicas.Where(t=> t.Id == id))
            {
                listaOtpremnica.Add(Map(aspNetOtpremnica));
            }
            return listaOtpremnica;
        }

        public int Update(int id)
        {
            int stornirano = 0;
            List<PakovanjeBO> listaPakovanja = new List<PakovanjeBO>();
            try
            {
                AspNetOtpremnica aspNetOtpremnica = databaseEntities.AspNetOtpremnicas.Single(t => t.Id == id);
                List<StavkaOtpremniceBO> listaStavki = (List<StavkaOtpremniceBO>)stavkaOtpremniceRepository.GetStavkeOtpremniceBO(id);
                if(listaStavki != null && listaStavki.Count > 0)
                {
                    foreach(StavkaOtpremniceBO stavkaOtpremniceBO in listaStavki)
                    {
                        PakovanjeBO pakovanjeBO = pakovanjeRepository.GetById(stavkaOtpremniceBO.IdPakovanje);
                        listaPakovanja.Add(pakovanjeBO);
                        int updatedPakovanje = pakovanjeRepository.Update(pakovanjeBO.Id, null);
                        if(updatedPakovanje > 0)
                        {
                            HladnjacaBO hladnjacaBO = hladnjacaRepository.GetById(pakovanjeBO.Hladnjaca.Id);
                            double newCapacity = hladnjacaBO.Kapacitet - pakovanjeBO.Kolicina;
                            int updatedHladnjaca = hladnjacaRepository.UpdateCapacity(hladnjacaBO.Id, newCapacity);
                            if(updatedHladnjaca <= 0)
                            {
                                throw new Exception();
                            }
                        } else
                        {
                            throw new Exception();
                        }
                    }
                    aspNetOtpremnica.DatumStorniranja = DateTime.Now;
                    stornirano = databaseEntities.SaveChanges();
                } 
                return stornirano;
            }
            catch (Exception)
            {
                if(listaPakovanja.Count > 0)
                {
                    foreach (PakovanjeBO pakovanjeBO in listaPakovanja)
                    {
                        pakovanjeRepository.Update(pakovanjeBO.Id, pakovanjeBO.DatumIzdavanja);
                        hladnjacaRepository.UpdateCapacity(pakovanjeBO.Hladnjaca.Id, pakovanjeBO.Hladnjaca.Kapacitet);
                    }
                }
                return stornirano;
            }
        }

        public OtpremnicaBO Map(AspNetOtpremnica aspNetOtpremnica)
        {
            OtpremnicaBO otpremnicaBO = new OtpremnicaBO
            {
                Id = aspNetOtpremnica.Id,
                Datum = aspNetOtpremnica.Datum,
                DatumStorniranja = aspNetOtpremnica.DatumStorniranja,
                UserBO = new UserBO
                {
                    Id = aspNetOtpremnica.AspNetUser.Id,
                    Ime = aspNetOtpremnica.AspNetUser.FirstName,
                    Prezime = aspNetOtpremnica.AspNetUser.LastName
                },
                PoslovniPartner = new PoslovniPartnerBO
                {
                    Id = aspNetOtpremnica.AspNetPoslovniPartner.Id,
                    Naziv = aspNetOtpremnica.AspNetPoslovniPartner.Naziv,
                    Adresa = aspNetOtpremnica.AspNetPoslovniPartner.Adresa
                },
                ListaStavki = (List<StavkaOtpremniceBO>)stavkaOtpremniceRepository.GetStavkeOtpremniceBO(aspNetOtpremnica.Id)
            };
            return otpremnicaBO;
        }

        public IEnumerable<StavkaOtpremniceBO> GetListStavke(int id)
        {
            throw new NotImplementedException();
        }
    }
}
