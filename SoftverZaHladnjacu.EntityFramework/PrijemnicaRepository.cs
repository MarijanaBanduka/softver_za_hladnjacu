﻿using SoftverZaHladnjacu.Domain;
using SoftverZaHladnjacu.Domain.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace SoftverZaHladnjacu.EntityFramework
{
    public class PrijemnicaRepository : IPrijemnicaRepository
    {
        private StorageDatabaseEntities databaseEntities = new StorageDatabaseEntities();
        private UserRepository userRepository = new UserRepository();
        private HladnjacaRepository hladnjacaRepository = new HladnjacaRepository();

        public int Create(PrijemnicaBO prijemnicaBO)
        {
            int created = 0;
            int saved = 0;
            AspNetPrijemnica aspNetPrijemnica = null;
            try
            {
                HladnjacaBO hladnjacaBO = hladnjacaRepository.GetById(prijemnicaBO.Hladnjaca.Id);
                    string username = HttpContext.Current.User.Identity.Name;
                    UserBO userBO = userRepository.GetUserByUsername(username);
                    aspNetPrijemnica = new AspNetPrijemnica
                    {
                        DatumPrijema = DateTime.Now,
                        Kolicina = prijemnicaBO.Kolicina,
                        SkladisniKalo = prijemnicaBO.Kolicina,
                        IdHladnjaca = prijemnicaBO.Hladnjaca.Id,
                        IdSorta = prijemnicaBO.Sorta.Id,
                        IdPoslovniPartner = prijemnicaBO.PoslovniPartner.Id,
                        IdRadnik = userBO.Id
                    };
                    databaseEntities.AspNetPrijemnicas.Add(aspNetPrijemnica);
                    saved = databaseEntities.SaveChanges();
                    if (saved > 0)
                    {
                        double newCapacity = hladnjacaBO.Kapacitet - prijemnicaBO.Kolicina;
                        //throw new Exception();
                        created = hladnjacaRepository.UpdateCapacity(hladnjacaBO.Id, newCapacity);
                    }
            }
            catch (Exception)
            {
                if (saved > 0)
                {
                    databaseEntities.AspNetPrijemnicas.Remove(aspNetPrijemnica);
                    databaseEntities.SaveChanges();
                }
                return created;
            }
            return created;
        }

        public int UpdateQuantity(PrijemnicaBO prijemnicaBO)
        {
            AspNetPrijemnica aspNetPrijemnica = databaseEntities.AspNetPrijemnicas.Single(t => t.Id == prijemnicaBO.Id);
            aspNetPrijemnica.SkladisniKalo = prijemnicaBO.SkladisniKalo;
            return databaseEntities.SaveChanges();
        }

        public int UpdateSetStored(int id)
        {
            int saved = 0;
            int updated = 0;
            AspNetPrijemnica aspNetPrijemnica = null;
            try
            {
                aspNetPrijemnica = databaseEntities.AspNetPrijemnicas.Single(t => t.Id == id);
                aspNetPrijemnica.DatumPakovanja = DateTime.Now;
                saved = databaseEntities.SaveChanges();
                int idHladnjace = aspNetPrijemnica.IdHladnjaca;
                if(saved > 0 )
                {
                    HladnjacaBO hladnjacaBO = hladnjacaRepository.GetById(idHladnjace);
                    double newCapacity = hladnjacaBO.Kapacitet + aspNetPrijemnica.Kolicina;
                    updated = hladnjacaRepository.UpdateCapacity(idHladnjace, newCapacity);
                }
                return updated;
            }
            catch (Exception)
            {
                if (saved > 0)
                {
                    aspNetPrijemnica.DatumPakovanja = null;
                    databaseEntities.SaveChanges();
                }
                return updated;
            }
        }

        public PrijemnicaBO GetById(int id)
        {
            AspNetPrijemnica aspNetPrijemnica = databaseEntities.AspNetPrijemnicas.Single(t => t.Id == id);
            return Map(aspNetPrijemnica);
        }

        public IEnumerable<PrijemnicaBO> GetListById(int id)
        {
            List<PrijemnicaBO> listaPrijemnica = new List<PrijemnicaBO>();
            foreach (AspNetPrijemnica aspNetPrijemnica in databaseEntities.AspNetPrijemnicas.Where(t => t.Id == id))
            {
                listaPrijemnica.Add(Map(aspNetPrijemnica));
            }
            return listaPrijemnica;
        }

        public IEnumerable<PrijemnicaBO> GetByDate(DateTime date)
        {
            List<PrijemnicaBO> listaPrijemnica = new List<PrijemnicaBO>();
            foreach(AspNetPrijemnica aspNetPrijemnica in databaseEntities.AspNetPrijemnicas.Where(t=>t.DatumPrijema == date))
            {
                listaPrijemnica.Add(Map(aspNetPrijemnica));
            }
            return listaPrijemnica;
        }

        public IEnumerable<PrijemnicaBO> GetBySorta(int id_sorte)
        {
            List<PrijemnicaBO> listaPrijemnica = new List<PrijemnicaBO>();
            foreach (AspNetPrijemnica aspNetPrijemnica in databaseEntities.AspNetPrijemnicas.Where(t => t.IdSorta == id_sorte).OrderBy(t=>t.DatumPrijema))
            {
                listaPrijemnica.Add(Map(aspNetPrijemnica));
            }
            return listaPrijemnica;
        }

        public PrijemnicaBO Map(AspNetPrijemnica aspNetPrijemnica)
        {
            PrijemnicaBO prijemnicaBO = new PrijemnicaBO
            {
                Id = aspNetPrijemnica.Id,
                Kolicina = aspNetPrijemnica.Kolicina,
                DatumPrijema = aspNetPrijemnica.DatumPrijema,
                SkladisniKalo = aspNetPrijemnica.SkladisniKalo,
                DatumPakovanja = aspNetPrijemnica.DatumPakovanja,
                Hladnjaca = new HladnjacaBO
                {
                    Id = aspNetPrijemnica.AspNetHladnjaca.Id,
                    Naziv = aspNetPrijemnica.AspNetHladnjaca.Naziv
                },
                Sorta = new SortaBO
                {
                    Id = aspNetPrijemnica.AspNetSorta.Id,
                    Naziv = aspNetPrijemnica.AspNetSorta.Naziv,
                },
                PoslovniPartner = new PoslovniPartnerBO
                {
                    Id = aspNetPrijemnica.AspNetPoslovniPartner.Id,
                    Naziv = aspNetPrijemnica.AspNetPoslovniPartner.Naziv,
                    Adresa = aspNetPrijemnica.AspNetPoslovniPartner.Adresa
                },
                UserId = aspNetPrijemnica.IdRadnik,
                UserBO = new UserBO
                {
                    Id = aspNetPrijemnica.AspNetUser.Id,
                    Ime = aspNetPrijemnica.AspNetUser.FirstName,
                    Prezime = aspNetPrijemnica.AspNetUser.LastName,
                    Email = aspNetPrijemnica.AspNetUser.Email
                }
                
            };
            return prijemnicaBO;
        }

        public IEnumerable<PrijemnicaBO> GetAllNotStored()
        {
            List<PrijemnicaBO> listaPrijemnica = new List<PrijemnicaBO>();
            foreach (AspNetPrijemnica aspNetPrijemnica in databaseEntities.AspNetPrijemnicas.Where(t => t.DatumPakovanja == null).OrderBy(t => t.DatumPrijema))
            {
                listaPrijemnica.Add(Map(aspNetPrijemnica));
            }
            return listaPrijemnica;
        }
    }
}
