﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SoftverZaHladnjacu.Domain;
using SoftverZaHladnjacu.Domain.Repository;

namespace SoftverZaHladnjacu.EntityFramework
{
    public class HladnjacaRepository : IHladnjacaRepository
    {
        private StorageDatabaseEntities databaseEntities = new StorageDatabaseEntities();
        public int Create(HladnjacaBO hladnjacaBO)
        {
            AspNetHladnjaca aspNetHladnjaca = new AspNetHladnjaca
            {
                Naziv = hladnjacaBO.Naziv,
                Kapacitet = hladnjacaBO.Kapacitet,
                Aktivna = hladnjacaBO.Aktivna,
                IdTipHladnjace = hladnjacaBO.IdTipHladnjace
            };
            databaseEntities.AspNetHladnjacas.Add(aspNetHladnjaca);
            return databaseEntities.SaveChanges();
        }

        public HladnjacaBO GetById(int id)
        {
            AspNetHladnjaca aspNetHladnjaca = databaseEntities.AspNetHladnjacas.Single(t => t.Id == id);
            return Map(aspNetHladnjaca);
        }

        public int Update(HladnjacaBO hladnjacaBO)
        {
            AspNetHladnjaca aspNetHladnjaca = databaseEntities.AspNetHladnjacas.Single(t => t.Id == hladnjacaBO.Id);
            aspNetHladnjaca.Naziv = hladnjacaBO.Naziv;
            aspNetHladnjaca.Kapacitet = hladnjacaBO.Kapacitet;
            aspNetHladnjaca.Aktivna = hladnjacaBO.Aktivna;
            return databaseEntities.SaveChanges();
        }
        public int UpdateCapacity(int id, double capacity)
        {
            AspNetHladnjaca aspNetHladnjaca = databaseEntities.AspNetHladnjacas.Single(t => t.Id == id);
            aspNetHladnjaca.Kapacitet = capacity;
            return databaseEntities.SaveChanges();
        }

        public IEnumerable<HladnjacaBO> GetAll()
        {
            List<HladnjacaBO> listaHladnjaca = new List<HladnjacaBO>();
            foreach(AspNetHladnjaca aspNetHladnjaca in databaseEntities.AspNetHladnjacas)
            {
                listaHladnjaca.Add(Map(aspNetHladnjaca));
            }
            return listaHladnjaca;
        }

        public IEnumerable<HladnjacaBO> GetAllTypePrijemna()
        {
            List<HladnjacaBO> listaHladnjaca = new List<HladnjacaBO>();
            foreach (AspNetHladnjaca aspNetHladnjaca in databaseEntities.AspNetHladnjacas.Where(t=>t.IdTipHladnjace == 2 && t.Aktivna == 1))
            {

                    listaHladnjaca.Add(Map(aspNetHladnjaca));
            }
            return listaHladnjaca;
        }

        public IEnumerable<HladnjacaBO> GetAllTypeGlavna()
        {
            List<HladnjacaBO> listaHladnjaca = new List<HladnjacaBO>();
            foreach (AspNetHladnjaca aspNetHladnjaca in databaseEntities.AspNetHladnjacas.Where(t => t.IdTipHladnjace == 1 && t.Aktivna == 1))
            {
                    listaHladnjaca.Add(Map(aspNetHladnjaca));
            }
            return listaHladnjaca;
        }

        public IEnumerable<TipHladnjaceBO> GetAllTypes()
        {
            List<TipHladnjaceBO> listaTipova = new List<TipHladnjaceBO>();
            foreach(AspNetTipHladnjace tipHladnjace in databaseEntities.AspNetTipHladnjaces)
            {
                TipHladnjaceBO tipHladnjaceBO = new TipHladnjaceBO
                {
                    Id = tipHladnjace.Id,
                    Naziv = tipHladnjace.Naziv
                };
                listaTipova.Add(tipHladnjaceBO);
            }
            return listaTipova;
        }

        public HladnjacaBO Map(AspNetHladnjaca aspNetHladnjaca)
        {
            HladnjacaBO hladnjacaBO = new HladnjacaBO
            {
                Id = aspNetHladnjaca.Id,
                Naziv = aspNetHladnjaca.Naziv,
                Kapacitet = aspNetHladnjaca.Kapacitet,
                Aktivna = aspNetHladnjaca.Aktivna,
                IdTipHladnjace = aspNetHladnjaca.IdTipHladnjace,
                TipHladnjace = new TipHladnjaceBO
                {
                    Id = aspNetHladnjaca.AspNetTipHladnjace.Id,
                    Naziv = aspNetHladnjaca.AspNetTipHladnjace.Naziv
                }
            };
            return hladnjacaBO;
        }
    }
}
