﻿using SoftverZaHladnjacu.Domain;
using SoftverZaHladnjacu.Domain.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SoftverZaHladnjacu.EntityFramework
{
    public class SortaRepository : ISortaRepository
    {
        private StorageDatabaseEntities databaseEntities = new StorageDatabaseEntities();

        public IEnumerable<SortaBO> GetAll()
        {
            List<SortaBO> listaSorti = new List<SortaBO>();
            foreach(AspNetSorta aspNetSorta in databaseEntities.AspNetSortas.OrderBy(t=>t.IdVoce))
            {
                SortaBO sortaBO = new SortaBO
                {
                    Id = aspNetSorta.Id,
                    Naziv = aspNetSorta.Naziv,
                    Voce = new VoceBO
                    {
                        Id = aspNetSorta.AspNetVoce.Id,
                        Naziv = aspNetSorta.AspNetVoce.Naziv
                    }
                };
                listaSorti.Add(sortaBO);
            }
            return listaSorti;
        }
    }
}
