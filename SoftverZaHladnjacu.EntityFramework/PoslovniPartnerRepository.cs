﻿using SoftverZaHladnjacu.Domain;
using SoftverZaHladnjacu.Domain.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SoftverZaHladnjacu.EntityFramework
{
    public class PoslovniPartnerRepository : IPoslovniPartnerRepository
    {
        private StorageDatabaseEntities databaseEntities = new StorageDatabaseEntities();

        public IEnumerable<PoslovniPartnerBO> GetDobavljaci()
        {
            List<PoslovniPartnerBO> listaDobavljaca = new List<PoslovniPartnerBO>();
            foreach(AspNetPoslovniPartner aspNetPoslovniPartner in databaseEntities.AspNetPoslovniPartners.Where(t => t.IdTipPoslovnogPartnera == 1 || t.IdTipPoslovnogPartnera == 3))
            {
                PoslovniPartnerBO poslovniPartnerBO = new PoslovniPartnerBO
                {
                    Id = aspNetPoslovniPartner.Id,
                    Naziv = aspNetPoslovniPartner.Naziv,
                    PIB = aspNetPoslovniPartner.PIB,
                    Adresa = aspNetPoslovniPartner.Adresa
                };
                listaDobavljaca.Add(poslovniPartnerBO);
            }
            return listaDobavljaca;
        }

        public IEnumerable<PoslovniPartnerBO> GetKupci()
        {
            List<PoslovniPartnerBO> listaKupaca = new List<PoslovniPartnerBO>();
            foreach (AspNetPoslovniPartner aspNetPoslovniPartner in databaseEntities.AspNetPoslovniPartners.Where(t => t.IdTipPoslovnogPartnera == 2 || t.IdTipPoslovnogPartnera == 3))
            {
                PoslovniPartnerBO poslovniPartnerBO = new PoslovniPartnerBO
                {
                    Id = aspNetPoslovniPartner.Id,
                    Naziv = aspNetPoslovniPartner.Naziv,
                    PIB = aspNetPoslovniPartner.PIB,
                    Adresa = aspNetPoslovniPartner.Adresa
                };
                listaKupaca.Add(poslovniPartnerBO);
            }
            return listaKupaca;
        }
    }
}
