﻿using SoftverZaHladnjacu.Domain;
using SoftverZaHladnjacu.Domain.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace SoftverZaHladnjacu.EntityFramework
{
    public class PakovanjeRepository : IPakovanjeRepository
    {
        private StorageDatabaseEntities databaseEntities = new StorageDatabaseEntities();
        private UserRepository userRepository = new UserRepository();
        private HladnjacaRepository hladnjacaRepository = new HladnjacaRepository();
        private PrijemnicaRepository prijemnicaRepository = new PrijemnicaRepository();
        public int Create(PakovanjeBO pakovanjeBO)
        {
            int created = 0;
            int saved = 0;
            int capacityChanged = 0;
            AspNetPakovanje aspNetPakovanje = null;
            double kapacitetHladnjace = 0;
            int idHladnjace = 0;
            try
            {
                HladnjacaBO hladnjacaBO = hladnjacaRepository.GetById(pakovanjeBO.Hladnjaca.Id);
                string username = HttpContext.Current.User.Identity.Name;
                UserBO userBO = userRepository.GetUserByUsername(username);
                PrijemnicaBO prijemnicaBO = prijemnicaRepository.GetById(pakovanjeBO.Id);
                aspNetPakovanje = new AspNetPakovanje
                {
                    DatumPakovanja = DateTime.Now,
                    Kolicina = pakovanjeBO.Kolicina,
                    IdHladnjaca = pakovanjeBO.Hladnjaca.Id,
                    IdKlasa = pakovanjeBO.Klasa.Id,
                    IdPrijemnica = pakovanjeBO.Id,
                    IdRadnik = userBO.Id
                };
                databaseEntities.AspNetPakovanjes.Add(aspNetPakovanje);
                saved = databaseEntities.SaveChanges();
                if(saved > 0)
                {
                    idHladnjace = hladnjacaBO.Id;
                    kapacitetHladnjace = hladnjacaBO.Kapacitet;
                    double newCapacity = kapacitetHladnjace - pakovanjeBO.Kolicina;
                    capacityChanged = hladnjacaRepository.UpdateCapacity(idHladnjace, newCapacity);
                    if(capacityChanged > 0)
                    {
                        double newKalo = Convert.ToDouble(prijemnicaBO.SkladisniKalo) - pakovanjeBO.Kolicina;
                        prijemnicaBO.SkladisniKalo = newKalo;
                        created = prijemnicaRepository.UpdateQuantity(prijemnicaBO);
                        if (created <= 0)
                        {
                            throw new Exception();
                        }
                    } else
                    {
                        throw new Exception();
                    }
                }
            }
            catch (Exception)
            {
                if(saved > 0)
                {
                    databaseEntities.AspNetPakovanjes.Remove(aspNetPakovanje);
                    databaseEntities.SaveChanges();
                }
                if(capacityChanged > 0)
                {
                    hladnjacaRepository.UpdateCapacity(idHladnjace, kapacitetHladnjace);
                }
                return created;
            }
            return created;
        }

        public int Update(int id, DateTime? datumIzdavanja)
        {
            AspNetPakovanje aspNetPakovanje = databaseEntities.AspNetPakovanjes.Single(t => t.Id == id);
            aspNetPakovanje.DatumIzdavanja = datumIzdavanja;
            return databaseEntities.SaveChanges();
        }

        public PakovanjeBO GetById(int id)
        {
            try
            {
                AspNetPakovanje aspNetPakovanje = databaseEntities.AspNetPakovanjes.Single(t => t.Id == id);
                return Map(aspNetPakovanje);
            }
            catch (Exception)
            {
                return null;
            }
        }

        public IEnumerable<PakovanjeBO> GetAll()
        {
            List<PakovanjeBO> listaPakovanja = new List<PakovanjeBO>();
            foreach (AspNetPakovanje aspNetPakovanje in databaseEntities.AspNetPakovanjes.OrderBy(t => t.DatumIzdavanja))
            {
                listaPakovanja.Add(Map(aspNetPakovanje));
            }
            return listaPakovanja;
        }

        public IEnumerable<PakovanjeBO> GetPakovanjaByHladnjaca(int id_hladnjace)
        {
            List<PakovanjeBO> listaPakovanja = new List<PakovanjeBO>();
            foreach (AspNetPakovanje aspNetPakovanje in databaseEntities.AspNetPakovanjes.Where(t => t.IdHladnjaca == id_hladnjace).OrderBy(t => t.DatumIzdavanja))
            {
                listaPakovanja.Add(Map(aspNetPakovanje));
            }
            return listaPakovanja;
        }

        public IEnumerable<PakovanjeBO> GetPakovanjaByKlasa(int id_klase)
        {
            List<PakovanjeBO> listaPakovanja = new List<PakovanjeBO>();
            foreach (AspNetPakovanje aspNetPakovanje in databaseEntities.AspNetPakovanjes.Where(t => t.IdKlasa == id_klase).OrderBy(t => t.DatumIzdavanja))
            {
                listaPakovanja.Add(Map(aspNetPakovanje));
            }
            return listaPakovanja;
        }

        public IEnumerable<PakovanjeBO> GetPakovanjaBySorta(int id_sorte)
        {
            List<PakovanjeBO> listaPakovanja = new List<PakovanjeBO>();
            foreach (AspNetPakovanje aspNetPakovanje in databaseEntities.AspNetPakovanjes.Where(t => t.AspNetPrijemnica.IdSorta == id_sorte).OrderBy(t=>t.DatumIzdavanja))
            {
                listaPakovanja.Add(Map(aspNetPakovanje));
            }
            return listaPakovanja;
        }

        public IEnumerable<KlasaBO> GetAllKlasa()
        {
            List<KlasaBO> listaKlasa = new List<KlasaBO>();
            foreach(AspNetKlasa aspNetKlasa in databaseEntities.AspNetKlasas)
            {
                KlasaBO klasaBO = new KlasaBO
                {
                    Id = aspNetKlasa.Id,
                    Naziv = aspNetKlasa.Naziv
                };
                listaKlasa.Add(klasaBO);
            }
            return listaKlasa;
        }

        public PakovanjeBO Map(AspNetPakovanje aspNetPakovanje)
        {
            PakovanjeBO pakovanjeBO = new PakovanjeBO
            {
                Id = aspNetPakovanje.Id,
                DatumPakovanja = aspNetPakovanje.DatumPakovanja,
                Kolicina = aspNetPakovanje.Kolicina,
                DatumIzdavanja = aspNetPakovanje.DatumIzdavanja,
                Hladnjaca = new HladnjacaBO
                {
                    Id = aspNetPakovanje.AspNetHladnjaca.Id,
                    Naziv = aspNetPakovanje.AspNetHladnjaca.Naziv,
                    Kapacitet = aspNetPakovanje.AspNetHladnjaca.Kapacitet
                },
                Klasa = new KlasaBO
                {
                    Id = aspNetPakovanje.AspNetKlasa.Id,
                    Naziv = aspNetPakovanje.AspNetKlasa.Naziv
                },
                Prijemnica = new PrijemnicaBO
                {
                    Id = aspNetPakovanje.AspNetPrijemnica.Id,
                    Sorta = new SortaBO
                    {
                        Id = aspNetPakovanje.AspNetPrijemnica.AspNetSorta.Id,
                        Naziv = aspNetPakovanje.AspNetPrijemnica.AspNetSorta.Naziv
                    }
                },
                UserBO = new UserBO
                {
                    Id = aspNetPakovanje.AspNetUser.Id,
                    Email = aspNetPakovanje.AspNetUser.Email,
                    Ime = aspNetPakovanje.AspNetUser.FirstName,
                    Prezime = aspNetPakovanje.AspNetUser.LastName
                }
            };
            return pakovanjeBO;
        }
    }
}
