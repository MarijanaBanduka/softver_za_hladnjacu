﻿using SoftverZaHladnjacu.Domain;
using SoftverZaHladnjacu.Domain.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SoftverZaHladnjacu.EntityFramework
{
    public class StavkaOtpremniceRepository : IStavkaOtpremniceRepository
    {
        private StorageDatabaseEntities databaseEntities = new StorageDatabaseEntities();
        public int Create(List<int> listaSifri, int id_otpremnice)
        {
            int rb = 1;
            foreach(int sifra in listaSifri)
            {
                AspNetStavkaOtpremnice aspNetStavkaOtpremnice = new AspNetStavkaOtpremnice
                {
                    Rb = rb,
                    IdPakovanje = sifra,
                    IdOtpremnica = id_otpremnice
                };
                rb++;
                databaseEntities.AspNetStavkaOtpremnices.Add(aspNetStavkaOtpremnice);
            }
            return databaseEntities.SaveChanges();
        }

        public IEnumerable<StavkaOtpremniceBO> GetStavkeOtpremniceBO(int id_otpremnice)
        {
            List<StavkaOtpremniceBO> listaStavki = new List<StavkaOtpremniceBO>();
            foreach (AspNetStavkaOtpremnice aspNetStavkaOtpremnice in databaseEntities.AspNetStavkaOtpremnices.Where(t => t.IdOtpremnica == id_otpremnice))
            {
                listaStavki.Add(Map(aspNetStavkaOtpremnice));
            }
            return listaStavki;
        }

        public StavkaOtpremniceBO Map(AspNetStavkaOtpremnice aspNetStavkaOtpremnice)
        {
            StavkaOtpremniceBO stavkaOtpremniceBO = new StavkaOtpremniceBO
            {
                Id = aspNetStavkaOtpremnice.Id,
                Rb = aspNetStavkaOtpremnice.Rb,
                IdPakovanje = aspNetStavkaOtpremnice.IdPakovanje,
                IdOtpremnica = aspNetStavkaOtpremnice.IdOtpremnica,
                Pakovanje = new PakovanjeBO
                {
                    Id = aspNetStavkaOtpremnice.AspNetPakovanje.Id,
                    DatumPakovanja = aspNetStavkaOtpremnice.AspNetPakovanje.DatumPakovanja,
                    Kolicina = aspNetStavkaOtpremnice.AspNetPakovanje.Kolicina,
                    Klasa = new KlasaBO
                    {
                        Naziv = aspNetStavkaOtpremnice.AspNetPakovanje.AspNetKlasa.Naziv
                    },
                    Prijemnica = new PrijemnicaBO
                    {
                        Sorta = new SortaBO
                        {
                            Naziv = aspNetStavkaOtpremnice.AspNetPakovanje.AspNetPrijemnica.AspNetSorta.Naziv,
                            Voce = new VoceBO
                            {
                                Naziv = aspNetStavkaOtpremnice.AspNetPakovanje.AspNetPrijemnica.AspNetSorta.AspNetVoce.Naziv
                            }
                        }
                    }
                }
            };
            return stavkaOtpremniceBO;
        }
    }
}
