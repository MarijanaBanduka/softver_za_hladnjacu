﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using SoftverZaHladnjacu.Client;
using SoftverZaHladnjacu.Client.Controllers;
using SoftverZaHladnjacu.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;

namespace SoftverZaHladnjacu.Tests.Controllers
{
    [TestClass]
    public class HladnjacaControllerTest
    {
        [TestMethod]
        public void Index()
        {
            HladnjacaController hladnjacaController = new HladnjacaController();
            ViewResult viewResult = hladnjacaController.Index() as ViewResult;
            Assert.IsNotNull(viewResult);
        }

        [TestMethod]
        public void GetAllHladnjacas()
        {
            HladnjacaController hladnjacaController = new HladnjacaController();
            PartialViewResult partialViewResult = hladnjacaController.GetAllHladnjacas() as PartialViewResult;
            Assert.IsNotNull(partialViewResult);
        }

        [TestMethod]
        public void Create()
        {
            HladnjacaController hladnjacaController = new HladnjacaController();
            ViewResult viewResult = hladnjacaController.Create() as ViewResult;
            Assert.IsNotNull(viewResult);
        }


        [TestMethod]
        public void Edit()
        {
            HladnjacaController hladnjacaController = new HladnjacaController();
            ViewResult viewResult = hladnjacaController.Edit(1) as ViewResult;
            HladnjacaBO hladnjacaBO = (HladnjacaBO)viewResult.ViewData.Model;
            Assert.AreEqual("PH - 001", hladnjacaBO.Naziv);
        }
    }
}
