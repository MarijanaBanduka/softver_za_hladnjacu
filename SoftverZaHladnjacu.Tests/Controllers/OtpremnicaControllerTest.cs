﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using SoftverZaHladnjacu.Client.Controllers;
using SoftverZaHladnjacu.Domain;
using System.Web.Mvc;

namespace SoftverZaHladnjacu.Tests.Controllers
{
    [TestClass]
    public class OtpremnicaControllerTest
    {
        [TestMethod]
        public void Index()
        {
            OtpremnicaController otpremnicaController = new OtpremnicaController();
            ViewResult viewResult = otpremnicaController.Index() as ViewResult;
            Assert.IsNotNull(viewResult);
        }

        [TestMethod]
        public void GetAllOtpremnicas()
        {
            OtpremnicaController otpremnicaController = new OtpremnicaController();
            PartialViewResult partialViewResult = otpremnicaController.GetAllOtpremnicas() as PartialViewResult;
            Assert.IsNotNull(partialViewResult);
        }

        [TestMethod]
        public void GetOtpremnicasById()
        {
            OtpremnicaController otpremnicaController = new OtpremnicaController();
            PartialViewResult partialViewResult = otpremnicaController.GetOtpremnicasById(11) as PartialViewResult;
            Assert.IsNotNull(partialViewResult);
        }

        [TestMethod]
        public void Create()
        {
            OtpremnicaController otpremnicaController = new OtpremnicaController();
            ViewResult viewResult = otpremnicaController.Create() as ViewResult;
            Assert.IsNotNull(viewResult);
        }

        [TestMethod]
        public void Details()
        {
            OtpremnicaController otpremnicaController = new OtpremnicaController();
            ViewResult viewResult = otpremnicaController.Details(10) as ViewResult;
            OtpremnicaBO otpremnicaBO = (OtpremnicaBO)viewResult.ViewData.Model;
            Assert.AreEqual(6, otpremnicaBO.PoslovniPartner.Id);
        }

        [TestMethod]
        public void Edit()
        {
            OtpremnicaController otpremnicaController = new OtpremnicaController();
            ViewResult viewResult = otpremnicaController.Edit(10) as ViewResult;
            string expectedMessage = "Došlo je do greške, otpremnica nije stornirana!";
            if (viewResult != null)
                Assert.AreEqual(expectedMessage, otpremnicaController.ViewBag.MessageF);
        }

    }
}
