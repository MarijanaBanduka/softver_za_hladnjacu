﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SoftverZaHladnjacu.Domain
{
    public class PrijemnicaBO
    {
        public int Id { get; set; }

        [Display(Name = "Datum prijema")]
        [DataType(DataType.Date)]
        public DateTime DatumPrijema { get; set; }

        [Display(Name = "Datum pakovanja")]
        [DataType(DataType.Date)]
        public DateTime? DatumPakovanja { get; set; }

        [Display(Name = "Količina")]
        [Required(ErrorMessage = "Morate uneti kolicinu")]
        public double Kolicina { get; set; }

        [Display(Name = "Skladišni kalo")]
        public double? SkladisniKalo { get; set; }

        public string UserId { get; set; }

        [Display(Name = "Dobavljač")]
        public PoslovniPartnerBO PoslovniPartner { get; set; }
        public SortaBO Sorta { get; set; }
        public HladnjacaBO Hladnjaca { get; set; }
        public UserBO UserBO { get; set; }
    }
}
