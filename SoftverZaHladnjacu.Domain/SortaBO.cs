﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SoftverZaHladnjacu.Domain
{
    public class SortaBO
    {
        [Required(ErrorMessage = "Morate odabrati sortu")]
        [Display(Name = "Sorta")]
        public int Id { get; set; }
        public string Naziv { get; set; }
        public VoceBO Voce { get; set; }

        [Display(Name = "Naziv voća i sorte")]
        public string Prikaz { get
            {
                return Voce.Naziv + " - " + Naziv;
            }
        }
    }
}
