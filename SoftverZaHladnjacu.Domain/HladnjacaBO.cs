﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SoftverZaHladnjacu.Domain
{
    public class HladnjacaBO
    {
        [Required(ErrorMessage = "Morate odabrati hladnjaču")]
        [Display(Name = "Hladnjača")]
        public int Id { get; set; }

        [Required(ErrorMessage = "Morate uneti naziv hladnjače.")]
        public string Naziv { get; set; }

        [Required(ErrorMessage = "Morate uneti kapacitet hladnjače.")]
        public double Kapacitet { get; set; }

        [Display(Name = "Status")]
        [Required(ErrorMessage = "Morate uneti status hladnjače.")]
        [Range(0, 1, ErrorMessage = "Morate uneti vrednost 0 ili 1")]
        public byte Aktivna { get; set; }

        [Display(Name = "Tip hladnjače")]
        [Required(ErrorMessage = "Morate odabrati tip hladnjače.")]
        public int IdTipHladnjace { get; set; }
        public TipHladnjaceBO TipHladnjace { get; set; }

        public string Prikaz { get {
                return Naziv + " - " + Kapacitet + "kg";
            } }
    }
}
