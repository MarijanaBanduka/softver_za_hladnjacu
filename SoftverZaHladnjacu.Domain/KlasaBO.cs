﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SoftverZaHladnjacu.Domain
{
    public class KlasaBO
    {
        [Display(Name = "Klasa")]
        [Required(ErrorMessage = "Morate odabrati klasu")]
        public int Id { get; set; }
        public string Naziv { get; set; }
    }
}
