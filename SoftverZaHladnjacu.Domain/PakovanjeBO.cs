﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SoftverZaHladnjacu.Domain
{
    public class PakovanjeBO
    {
        [Display(Name = "Šifra")]
        public int Id { get; set; }

        [Display(Name = "Datum pakovanja")]
        [DataType(DataType.Date)]
        public DateTime DatumPakovanja { get; set; }

        [Display(Name = "Količina")]
        [Required(ErrorMessage = "Morate uneti količinu")]
        public double Kolicina { get; set; }

        [Display(Name = "Datum izdavanja")]
        [DataType(DataType.Date)]
        public DateTime? DatumIzdavanja { get; set; }


        public HladnjacaBO Hladnjaca { get; set; }
        public KlasaBO Klasa { get; set; }
        public PrijemnicaBO Prijemnica { get; set; }
        public UserBO UserBO { get; set; }
    }
}
