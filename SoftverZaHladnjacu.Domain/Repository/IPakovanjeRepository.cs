﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SoftverZaHladnjacu.Domain.Repository
{
    public interface IPakovanjeRepository
    {
        int Create(PakovanjeBO pakovanjeBO);
        int Update(int id, DateTime? datumIzdavanja);
        PakovanjeBO GetById(int id);

        IEnumerable<PakovanjeBO> GetAll();
        IEnumerable<PakovanjeBO> GetPakovanjaByHladnjaca(int id_hladnjace);
        IEnumerable<PakovanjeBO> GetPakovanjaByKlasa(int id_klase);
        IEnumerable<PakovanjeBO> GetPakovanjaBySorta(int id_sorte);
        IEnumerable<KlasaBO> GetAllKlasa();
    }
}
