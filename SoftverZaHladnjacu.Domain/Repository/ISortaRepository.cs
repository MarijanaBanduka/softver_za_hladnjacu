﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SoftverZaHladnjacu.Domain.Repository
{
    public interface ISortaRepository
    {
        IEnumerable<SortaBO> GetAll();
    }
}
