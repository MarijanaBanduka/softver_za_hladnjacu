﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SoftverZaHladnjacu.Domain.Repository
{
    public interface IPrijemnicaRepository
    {
        int Create(PrijemnicaBO prijemnicaBO);
        int UpdateQuantity(PrijemnicaBO prijemnicaBO);
        int UpdateSetStored(int id);

        PrijemnicaBO GetById(int id);

        IEnumerable<PrijemnicaBO> GetListById(int id);
        IEnumerable<PrijemnicaBO> GetByDate(DateTime date);
        IEnumerable<PrijemnicaBO> GetBySorta(int id_sorte);
        IEnumerable<PrijemnicaBO> GetAllNotStored();
    }
}
