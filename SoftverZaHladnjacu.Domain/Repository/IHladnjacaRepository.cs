﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SoftverZaHladnjacu.Domain.Repository
{
    public interface IHladnjacaRepository
    {
        int Create(HladnjacaBO hladnjacaBO);
        int Update(HladnjacaBO hladnjacaBO);
        int UpdateCapacity(int id, double capacity);
        HladnjacaBO GetById(int id);
        IEnumerable<HladnjacaBO> GetAll();
        IEnumerable<HladnjacaBO> GetAllTypePrijemna();
        IEnumerable<HladnjacaBO> GetAllTypeGlavna();
        IEnumerable<TipHladnjaceBO> GetAllTypes();
    }
}
