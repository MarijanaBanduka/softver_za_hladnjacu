﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SoftverZaHladnjacu.Domain.Repository
{
    public interface IOtpremnicaRepository
    {
        int Create(OtpremnicaBO otpremnicaBO);
        int Update(int id);

        OtpremnicaBO GetById(int id);

        IEnumerable<OtpremnicaBO> GetListById(int id);
        IEnumerable<OtpremnicaBO> GetAll();

        IEnumerable<StavkaOtpremniceBO> GetListStavke(int id);
    }
}
