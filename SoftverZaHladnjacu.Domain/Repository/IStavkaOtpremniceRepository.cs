﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SoftverZaHladnjacu.Domain.Repository
{
    public interface IStavkaOtpremniceRepository
    {
        int Create(List<int> listaSifri, int id_otpremnice);

        IEnumerable<StavkaOtpremniceBO> GetStavkeOtpremniceBO(int id_otpremnice);
    }
}
