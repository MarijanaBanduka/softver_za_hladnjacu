﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SoftverZaHladnjacu.Domain
{
    public class StavkaOtpremniceBO
    {
        [Display(Name = "Stavka otpremnice")]
        public int Id { get; set; }

        [Display(Name = "Redni broj")]
        public int Rb { get; set; }

        public int IdPakovanje { get; set; }
        public int IdOtpremnica { get; set; }

        public PakovanjeBO Pakovanje { get; set; }
    }
}
