﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SoftverZaHladnjacu.Domain
{
    public class OtpremnicaBO
    {
        [Display(Name = "Šifra")]
        public int Id { get; set; }


        [Display(Name = "Datum izdavanja")]
        [DataType(DataType.Date)]
        public DateTime Datum { get; set; }


        [Display(Name = "Datum storniranja")]
        [DataType(DataType.Date)]
        public DateTime? DatumStorniranja { get; set; }
        
        public UserBO UserBO { get; set; }

        [Display(Name = "Kupac")]
        public PoslovniPartnerBO PoslovniPartner { get; set; }
        public List<StavkaOtpremniceBO> ListaStavki { get; set; }
        public List<int> ListaSifri { get; set; }
    }
}
