﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SoftverZaHladnjacu.Domain
{
    public class PoslovniPartnerBO
    {
        [Required(ErrorMessage = "Morate odabrati poslovnog partnera")]
        public int Id { get; set; }
        public string Naziv { get; set; }
        public string PIB { get; set; }
        public string Adresa { get; set; }
        public TipPoslovnogPartneraBO TipPoslovnogPartnera { get; set; }

        [Display(Name = "Poslovni partner")]
        public string Prikaz { get {
                return Naziv + " - " + Adresa;
            } }
    }
}
